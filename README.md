# TryLoveMondays
​
[![Build Status](https://travis-ci.org/alexandreprates/try_love_mondays.svg?branch=master)](https://travis-ci.org/alexandreprates/try_love_mondays)  [![Coverage Status](https://coveralls.io/repos/github/alexandreprates/try_love_mondays/badge.svg?branch=master)](https://coveralls.io/github/alexandreprates/try_love_mondays?branch=master)  [![Inline docs](http://inch-ci.org/github/alexandreprates/try_love_mondays.svg?branch=master)](http://inch-ci.org/github/alexandreprates/try_love_mondays)
​
​
## How to Install/Run
​
Clone the project and into it's directory you can run using this commands:
​
```shell
 $ bundle install
 $ bundle exec ./bin/play_parser
```
​
You can also build and install as gem:
​
```shell
 $ gem build try_love_mondays.gemspec
 $ gem install try_love_mondays
```
​
The advantage of using it as gem is that you can call _play_parser_ from anywhere
​
### Aditional options

You can run _play_parser_ with some options:

```shell
  $ play_parser --help
  Usage: play_parser [options]
      -a, --all                        Display stats for all characters
      -n, --name                       Sort results by name
      -s, --speeches                   Sort results by speeches
      -u, --url=URL                    Url for different play (Macbeth is the default)
      -v, --version                    Show version
      -h, --help                       Prints this help
```

## How to test
​
In project's folder run:
​
```shell
  $ bundle exec rake
```
​
Also if you want to run only tests:
​
```shell
  $ bundle exec rake rspec
```
​
## Libraies
​
For XML parsing I'm using [OX](https://github.com/ohler55/ox) because he is faster than 
[Nokogiri](https://github.com/sparklemotion/nokogiri).

I'm using [Faraday](https://github.com/lostisland/faraday) as http client because it's
simple to use and offers support to stubs results that make tests more easy and reliable.

For testing I'm using rspec as main test suite with [simplecov](https://github.com/colszowka/simplecov) and [coveralls](https://coveralls.io) to check coverage
and finally [guard](https://github.com/guard/guard) to automatically run tests.

## License
​
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
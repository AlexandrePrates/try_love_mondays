require 'spec_helper'

describe TryLoveMondays do
  it 'has a version number' do
    expect(TryLoveMondays::VERSION).not_to be nil
  end

  describe '#process' do
    let(:url) { 'http://test.com/test' }
    let(:response) { double 'faraday::response', :success? => true, :body => File.read('./spec/support/sample.xml') }

    it 'fetch, parse and show' do
      expect(TryLoveMondays::HttpClient).to receive(:get).with(url).and_return(response)
      expect { subject.process(url) }.to output("  LOREM................. 5\n  DONEC................. 2\n  ETIAM................. 3\n").to_stdout
    end

    it 'exits with code 1 when something goes wrong' do
      expect(TryLoveMondays::HttpClient).to receive(:get).with(url) { fail "boom" }
      expect { subject.process url }.to raise_error(SystemExit)
    end
  end
end

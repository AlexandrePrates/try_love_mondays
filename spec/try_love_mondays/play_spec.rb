require 'spec_helper'

describe TryLoveMondays::Play do
  subject { described_class.new }
  let(:char) { double('character', name: 'FOO') }

  it { is_expected.to respond_to :anybody }

  describe '#add' do
    it 'adding char to play' do
      subject.add char
      expect(subject.characters).to eq [char]
    end

    it 'returns added char' do
      expect(subject.add char).to eq char
    end
  end

  describe '#characters' do
    it 'retrives char by name' do
      subject.add char
      expect(subject.character('FOO')).to be char
    end

    it 'returns nil if not found' do
      expect(subject.character('ops!')).to be nil
    end
  end

  describe '#stats' do
    before do
      subject.add double('character', name: 'FOO', lines: 3)
      subject.add double('character', name: 'BAR', lines: 10)
      subject.add double('character', name: 'NEP', lines: 1)
    end

    it 'displays in order of appearance' do
      expect(subject.stats).to eq "  FOO................... 3\n  BAR................... 10\n  NEP................... 1"
    end

    it 'displays in order of name' do
      expect(subject.stats(:name)).to eq "  BAR................... 10\n  FOO................... 3\n  NEP................... 1"
    end

    it 'displays in order of lines' do
      expect(subject.stats(:lines)).to eq "  NEP................... 1\n  FOO................... 3\n  BAR................... 10"
    end
  end

  describe '#member?' do
    it 'returns false for ALL' do
      expect(subject.member? 'ALL').to eq false
    end

    it 'always accept name in uppercase' do
      expect(subject.member? 'FOO').to eq true
    end

    it 'accepts name in downcase if _anybody_ is active' do
      play = described_class.new(true)
      expect(play.member? 'foo').to be true
    end

    it 'rejects name in downcase' do
      expect(subject.member? 'foo').to be false
    end
  end

end

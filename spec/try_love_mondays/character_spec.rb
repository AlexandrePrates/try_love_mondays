require 'spec_helper'

describe TryLoveMondays::Character do
  subject(:character) { described_class.new('foo') }

  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:lines) }

  describe '#add_line' do
    it 'increase by one (default)' do
      character.add_line
      expect(character.lines).to eq 1
    end

    it 'increase by many' do
      character.add_line 5
      expect(character.lines).to be 5
    end
  end

  describe '#lines' do
    it 'starts as zero' do
      expect(character.lines).to be 0
    end

    it 'keeps lines count' do
      character.add_line 1
      character.add_line 1
      character.add_line 1
      expect(character.lines).to be 3
    end
  end
end

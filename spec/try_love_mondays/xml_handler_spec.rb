require 'spec_helper'

describe TryLoveMondays::XmlHandler do
  let(:play) { double('play') }
  let(:character_class) { double('character_class') }
  let(:nep) { double('character', name: 'FOO', lines: 0) }

  subject(:subject) { described_class.new(play, character_class) }

  it { is_expected.to respond_to(:current_tag) }
  it { is_expected.to respond_to(:current_character) }

  describe '#start_element' do
    it 'sets current_tag' do
      subject.start_element(:SPEECH)
      expect(subject.current_tag).to eq(:SPEECH)
    end
  end

  describe '#end_element' do
    it 'sets current_tag as nil' do
      subject.current_tag = :LINE
      subject.end_element(:LINE)
      expect(subject.current_tag).to be nil
    end

    it 'sets current_character to nil when SPEECH ends' do
      subject.current_character = nep
      subject.end_element(:LINE)
      expect(subject.current_character).to be nep
      subject.end_element(:SPEECH)
      expect(subject.current_character).to be nil
    end
  end

  describe '#process' do
    it 'sets current character' do
      expect(play).to receive(:character).with('FOO').and_return(nep)
      expect(play).to receive(:member?).with('FOO').and_return(true)

      subject.start_element(:SPEAKER)
      subject.process('FOO')
    end

    it 'creates new character when not found' do
      expect(play).to receive(:character).with('FOO').and_return(nil)
      expect(play).to receive(:member?).with('FOO').and_return(true)
      expect(play).to receive(:add).with(nep).and_return(nep)
      expect(character_class).to receive(:new).with('FOO').and_return(nep)

      subject.start_element(:SPEAKER)
      subject.process('FOO')
    end

    it 'increases current character lines' do
      expect(nep).to receive(:add_line)
      subject.current_character = nep

      subject.start_element(:LINE)
      subject.process('character\'s line')
    end
  end

end

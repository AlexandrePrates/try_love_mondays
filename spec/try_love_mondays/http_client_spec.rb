require 'spec_helper'

describe TryLoveMondays::HttpClient do

  describe '.client' do
    it 'returns a Faraday instance' do
      expect(subject.client).to be_kind_of(Faraday::Connection)
    end

    it 'always returns the same instance' do
      expect(subject.client).to be subject.client
    end

  end

  describe '.get' do
    before(:all) do
      described_class.client.adapter :test, Faraday::Adapter::Test::Stubs.new do |stub|
        stub.get('http://test.com/test') { |env| [200, {}, 'works'] }
      end
    end

    it 'performs http request' do
      response = subject.get('http://test.com/test')
      expect(response).to be_success
      expect(response.body).to eq 'works'
    end
  end
end

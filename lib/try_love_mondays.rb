require "try_love_mondays/version"
require 'try_love_mondays/http_client'
require 'try_love_mondays/character'
require 'try_love_mondays/play'
require 'try_love_mondays/xml_handler'

# Parser for XML Play with speech count
module TryLoveMondays
  extend self

  # Fetch, Parse and Output Play's speeches stats
  # @param url [String] play xml url
  # @param options [Hash] options for parse:
  #   :all -> Display all characters
  #   :sort -> Change stats sort
  def process(url, options = {})
    request = HttpClient.get(url)
    fail "can't fetch xml data #{request.status}" unless request.success?

    play = Play.new options[:all]
    handler = XmlHandler.new play, Character

    Ox.sax_parse(handler, request.body)

    stats = play.stats options[:sort]
    puts stats
  rescue Exception => e
    puts "Ops! #{e.message}"
    exit 1
  end

end

require 'ox'

module TryLoveMondays
  # XML {https://en.wikipedia.org/wiki/Simple_API_for_XML Sax} Handler with {https://github.com/ohler55/ox OX}
  #
  # Handles Sax events, creating play's characters and increase lines
  #
  # Example of the structure and when the events are triggered
  #
  #   <SPEAKER>                               <-- call _start_element_ with :SPEAKER
  #     speaker's name                        <-- call _process_ with "speaker\'s name"
  #   </SPEAKER>                              <-- call _end_element_ with :SPEAKER
  #
  #   <SPEECH>
  #     <SPEAKER>speaker's name</SPEAKER>      <-- Sets current character by name
  #     <LINE>speech line</LINE>               <-- Increase current character lines
  #     <LINE>speech line</LINE>               <-- Increase current character lines
  #   </SPEECH>                                <-- Set current character as nil
  class XmlHandler < ::Ox::Sax
    # @return [String] current tag name
    attr_accessor :current_tag
    # @return [Character] current character
    attr_accessor :current_character

    # Create new XmlHandler
    # @param play [Play] a play instance
    # @param character_creator [Character] character creator class
    def initialize(play, character_creator)
      @play = play
      @character_creator = character_creator
    end

    # Sax callback trigger for new element
    # @param name [String] element name
    def start_element(name)
      @current_tag = name
    end

    # Sax callback trigger for element's end
    # @param name [String] element name
    def end_element(name)
      @current_tag = nil
      @current_character = nil if name == :SPEECH
    end

    # Sax callback trigger for element content
    # @param value [String] element text content
    def process(value)
      case @current_tag
      when :SPEAKER
        set_current_character value if @play.member? value
      when :LINE
        current_character.add_line if current_character
      end
    end
    alias_method :text, :process

    private

    # Setter for current_character
    # When character not found in play they creates a new one
    # @param name [String] character name
    def set_current_character(name)
      @current_character = @play.character(name) || create_character(name)
    end

    # Create a new character and adding to play's characters list
    # @param name [String] the character's name
    def create_character(name)
      character = @character_creator.new name
      @play.add character
    end
  end
end
module TryLoveMondays
  # Class to collect all charaters and display play's speeches
  # @example Using
  #   play = Play.new #=> #<Play...>
  #   play.add important_character #=> #<Character...>
  #
  #   play.character 'important_character_name' #=> #<Character...>
  #   play.character 'wrong or missing name' #=> nil
  #
  #   play.cast #=> [#<Character..>]
  class Play
    # @return [Boolean] add anybody characters to play
    attr_reader :anybody

    # Create new (TryLoveMondays::Play) instance
    # @param anybody [Boolean] set as True to include any charater to play
    def initialize(anybody = false)
      @anybody = anybody
      @characters = {}
    end

    # Adding character to play's cast
    # @param [Character] character to be add to cast
    # @return [Character] added character
    def add(character)
      @characters[character.name] = character
    end

    # Retrive character from cast by name
    # @param [String] name of character
    def character(name)
      @characters[name]
    end

    # List all play's characters
    # @return [Array] with all characters
    def characters
      @characters.values
    end

    # Display play stats
    # @param sort [Symbol] sort results by
    #   nil (default) => Order by character appearance
    #   :name         => Order by character name
    #   :lines        => Order by character lines
    def stats(sort = nil)
      list = case sort
      when :name
        characters.sort_by &:name
      when :lines
        characters.sort_by &:lines
      else
        characters
      end
      list.collect { |char| "  #{char.name.ljust(22, '.')} #{char.lines}" }.join("\n")
    end

    # @return [Boolean] Return true if character has valid name
    def member?(name)
      return false if name == 'ALL'
      anybody || !name.match(/\p{Lower}/)
    end
  end
end
require 'faraday'

module TryLoveMondays
  # Wrapper from HTTP Client {https://github.com/lostisland/faraday Faraday}
  # @example Getting a page
  #   response = TryLoveMondays::HttpClient.get 'http://test.com/test' # => #<Faraday::Response...>
  #   response.success? # => True
  #   response.headers # => {headers data}
  #   response.body # => "body data"
  #
  module HttpClient
    extend self

    # Gets client
    # @return [Faraday::Connection] Http client
    def client
      @client ||= Faraday.new
    end

    # Makes GET request to url
    # @param [String] url
    # @return [Faraday::Respose] request response
    def get(url)
      client.get url
    end
  end
end
module TryLoveMondays
  # Class to count how many lines each character has in the play
  #
  # @example Character lines count
  #   char = Character.new 'NEP' #=> #<Character...>
  #   char.lines #=> 0
  #   char.add_line #=> 1
  #   char.add_line 3 #=> 4
  #   char.lines #=> 4
  class Character
    # @return [String] character's name
    attr_reader :name
    # @return [Integer] number of lines in the play
    attr_reader :lines

    # Create new {Character}
    # @param name [String] character's name
    def initialize(name)
      @name = name
      @lines = 0
    end

    # Increases lines count
    # @param count [Integer] <optional>
    # @return [Integer] current character lines count
    def add_line(count = 1)
      @lines += count
    end
  end
end